from aiogram import types, Dispatcher
from config import bot, cur, base
from litebot_keyboard import kb_client, kb_client_menu, kb_client_info
from litebot_data_base import sqlite_db
from aiogram.dispatcher.filters import Text

user_ID = None


async def command_start(message: types.Message):
    try:
        await bot.send_message(message.from_user.id, "Привет! Я тестовый бот и служу для \
                                                     демонстрации возможностей ", reply_markup=kb_client)
    except:
        await message.reply(str('К сожалению бот не может писать первым:(\
            \n Напишите ему t.me/lite_ver_bot'))
    finally:
        user_ID = message.from_user.id
        await sql_add_client_ID(user_ID)
        try:
            await message.delete()
        except:
            pass


async def info(message: types.Message):
    await bot.send_message(message.from_user.id, "В этом разделе будет располагаться информация \
                                                 о вашей компании", reply_markup=kb_client_info)


async def connect_for_user(message: types.Message):
    await bot.send_message(message.from_user.id, "Тут может быть контактная информация")


async def time_command(message: types.Message):
    await bot.send_message(message.from_user.id, 'Тут расписание работы')


async def place_command(message: types.Message):
    await bot.send_message(message.from_user.id, 'А тут месторасположение')


async def menu_command(message: types.Message):
    await bot.send_message(message.from_user.id, 'Тут вы можете выбрать раздел меню', reply_markup=kb_client_menu)


async def pizza_command(message: types.Message):
    await sqlite_db.sql_readpizza(message)


async def drink_command(message: types.Message):
    await sqlite_db.sql_readdrink(message)


async def sale_command(message: types.Message):
    await sqlite_db.sql_readsale(message)


async def sql_add_client_ID(user_ID):
    cur.execute('COMMIT;')
    cur.execute('BEGIN;')
    data = f'{user_ID}'
    cur.execute('SELECT id FROM client_id_base WHERE id = %s', (data,))
    res = cur.fetchone()
    base.commit()
    print(res)
    if res is None:
        cur.execute('INSERT INTO client_id_base VALUES(%s,%s)', (data, 1,))
        base.commit()
    else:
        cur.execute('UPDATE client_id_base SET count = count + 1 WHERE id = %s', (data,))
        base.commit()


def register_hnd_client(dp: Dispatcher):
    dp.register_message_handler(command_start, commands=["start", "help", "Назад"])
    dp.register_message_handler(info, commands=["Информация"])
    dp.register_message_handler(connect_for_user, commands=["Контактная_информация"])
    dp.register_message_handler(place_command, commands=["Расположение"])
    dp.register_message_handler(time_command, commands=["Расписание"])
    dp.register_message_handler(pizza_command, commands=["Пицца"])
    dp.register_message_handler(drink_command, commands=["Напитки"])
    dp.register_message_handler(sale_command, commands=["Акции"])
    dp.register_message_handler(menu_command, commands=["Меню"])
    dp.register_callback_query_handler(command_start, Text(equals="Назад"))
    dp.register_callback_query_handler(info, Text(equals="Информация"))
    dp.register_callback_query_handler(connect_for_user, Text(equals="Контактная информация"))
    dp.register_callback_query_handler(place_command, Text(equals="Расположение"))
    dp.register_callback_query_handler(time_command, Text(equals="Расписание"))
    dp.register_callback_query_handler(pizza_command, Text(equals="Пицца"))
    dp.register_callback_query_handler(drink_command, Text(equals="Напитки"))
    dp.register_callback_query_handler(sale_command, Text(equals="Акции"))
    dp.register_callback_query_handler(menu_command, Text(equals="Меню"))
