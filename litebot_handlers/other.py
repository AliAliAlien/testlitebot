from aiogram import types, Dispatcher

import json
import string


async def cenz(message: types.Message):
    if {i.lower().translate(str.maketrans("", "", string.punctuation)) for i in message.text.split(' ')} \
            .intersection(set(json.load(open('cenz.json')))) != set():
        await message.reply('Маты запрещены!')
        await message.delete()


def register_hnd_other(dp: Dispatcher):
    dp.register_message_handler(cenz)
