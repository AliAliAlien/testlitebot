from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types, Dispatcher
from aiogram.dispatcher.filters import Text
from config import bot
from litebot_data_base import sqlite_db
from litebot_keyboard import admin_kb
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

ID = None
check_status_FSM = None


class FSMAdmin(StatesGroup):
    photo = State()
    name = State()
    description = State()
    price = State()


async def make_changes_command(message: types.Message):
    global ID
    ID = message.from_user.id
    await bot.send_message(message.from_user.id, 'Бот готов к изменениям', reply_markup=admin_kb.kb_admin)
    await message.delete()


async def command_pizza_start(message: types.Message):
    if message.from_user.id == ID:
        global check_status_FSM
        check_status_FSM = "pizza"
        await FSMAdmin.photo.set()
        await message.reply('Загрузите фото')


async def command_drink_start(message: types.Message):
    if message.from_user.id == ID:
        global check_status_FSM
        check_status_FSM = 'drink'
        await FSMAdmin.photo.set()
        await message.reply('Загрузите фото напитка')


async def command_sale_start(message: types.Message):
    if message.from_user.id == ID:
        global check_status_FSM
        check_status_FSM = 'sale'
        await FSMAdmin.photo.set()
        await message.reply('Загрузите фото акции')


async def cancel_handler(message: types.Message, state=FSMContext):
    if message.from_user.id == ID:
        current_state = await state.get_state()
        if current_state is None:
            return
        await state.finish()
        await message.reply('OK')


async def load_photo(message: types.Message, state: FSMContext):
    if check_status_FSM == "pizza":
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['photo'] = message.photo[0].file_id
            await FSMAdmin.next()
            await message.reply('Введите название')
    if check_status_FSM == 'drink':
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['photo'] = message.photo[0].file_id
            await FSMAdmin.next()
            await message.reply('Введите название напитка')
    if check_status_FSM == 'sale':
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['photo'] = message.photo[0].file_id
            await FSMAdmin.next()
            await message.reply('Введите название акции')


# @dp.message_handler(state=FSMAdmin.name)


async def load_name(message: types.Message, state: FSMContext):
    if check_status_FSM == "pizza":
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['name'] = message.text
            await FSMAdmin.next()
            await message.reply('Введите описание')
    if check_status_FSM == 'drink':
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['name'] = message.text
            await FSMAdmin.next()
            await message.reply('Введите описание напитка')
    if check_status_FSM == 'sale':
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['name'] = message.text
            await FSMAdmin.next()
            await message.reply('Введите описание акции')


# @dp.message_handler(state=FSMAdmin.description)
async def load_description(message: types.Message, state: FSMContext):
    if check_status_FSM == "pizza":
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['description'] = message.text
            await FSMAdmin.next()
            await message.reply('Укажите цену')
    if check_status_FSM == 'drink':
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['description'] = message.text
            await FSMAdmin.next()
            await message.reply('Укажите цену напитка')
    if check_status_FSM == 'sale':
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['description'] = message.text
            await FSMAdmin.next()
            await message.reply('Укажите цену акции')


# @dp.message_handler(state=FSMAdmin.price)
async def load_price(message: types.Message, state: FSMContext):
    if check_status_FSM == "pizza":
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['price'] = message.text
            await sqlite_db.sql_addpizza_commands(state)
            await state.finish()
    if check_status_FSM == 'drink':
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['price'] = message.text
            await sqlite_db.sql_adddrink_commands(state)
            await state.finish()
    if check_status_FSM == 'sale':
        if message.from_user.id == ID:
            async with state.proxy() as data:
                data['price'] = message.text
            await sqlite_db.sql_addsale_commands(state)
            await state.finish()


async def delete_pizza(message: types.Message):
    if message.from_user.id == ID:
        read = await sqlite_db.sql_readpizza_admin()
        for ret in read:
            await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nОписание: {ret[2]}\nЦена: {ret[-1]}')
            await bot.send_message(message.from_user.id, text='^^^', reply_markup=InlineKeyboardMarkup().
                                   add(InlineKeyboardButton(f'Удалить {ret[1]}', callback_data=f'p del {ret[1]}')))


async def delete_pizza_callback_run(callback_query: types.CallbackQuery):
    await sqlite_db.sql_deletepizza_command(callback_query.data.replace('p del ', ''))
    await callback_query.answer(text=f'{callback_query.data.replace("p del ", "")} удалена.', show_alert=True)


async def delete_drink(message: types.Message):
    if message.from_user.id == ID:
        read = await sqlite_db.sql_readdrink_admin()
        for ret in read:
            await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nОписание: {ret[2]}\nЦена: {ret[-1]}')
            await bot.send_message(message.from_user.id, text='^^^', reply_markup=InlineKeyboardMarkup().
                                   add(InlineKeyboardButton(f'Удалить {ret[1]}', callback_data=f'd del {ret[1]}')))


async def delete_drink_callback_run(callback_query: types.CallbackQuery):
    await sqlite_db.sql_deletedrink_command(callback_query.data.replace('d del ', ''))
    await callback_query.answer(text=f'{callback_query.data.replace("d del ", "")} удалена.', show_alert=True)


async def delete_sale(message: types.Message):
    if message.from_user.id == ID:
        read = await sqlite_db.sql_readsale_admin()
        for ret in read:
            await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nОписание: {ret[2]}\nЦена: {ret[-1]}')
            await bot.send_message(message.from_user.id, text='^^^', reply_markup=InlineKeyboardMarkup().
                                   add(InlineKeyboardButton(f'Удалить {ret[1]}', callback_data=f's del {ret[1]}')))


async def delete_sale_callback_run(callback_query: types.CallbackQuery):
    await sqlite_db.sql_deletesale_command(callback_query.data.replace('s del ', ''))
    await callback_query.answer(text=f'{callback_query.data.replace("s del ", "")} удалена.', show_alert=True)


def register_hnd_admin(dp: Dispatcher):
    dp.register_message_handler(command_pizza_start, commands=["Загрузить_пиццу"], state=None)
    dp.register_message_handler(command_drink_start, commands=["Загрузить_напиток"], state=None)
    dp.register_message_handler(command_sale_start, commands=["Загрузить_акцию"], state=None)
    dp.register_message_handler(cancel_handler, state="*", commands="Отмена")
    dp.register_message_handler(cancel_handler, Text(equals="Отмена", ignore_case=None), state="*")
    dp.register_message_handler(load_photo, content_types=['photo'], state=FSMAdmin.photo)
    dp.register_message_handler(load_name, state=FSMAdmin.name)
    dp.register_message_handler(load_description, state=FSMAdmin.description)
    dp.register_message_handler(load_price, state=FSMAdmin.price)
    dp.register_message_handler(make_changes_command, commands=["moderator"], is_chat_admin=True)
    dp.register_message_handler(delete_drink, commands="Удалить_напиток")
    dp.register_message_handler(delete_pizza, commands="Удалить_пиццу")
    dp.register_message_handler(delete_sale, commands="Удалить_акцию")
    dp.register_callback_query_handler(delete_pizza_callback_run, Text(startswith='p del '))
    dp.register_callback_query_handler(delete_drink_callback_run, Text(startswith='d del '))
    dp.register_callback_query_handler(delete_sale_callback_run, Text(startswith='s del '))
