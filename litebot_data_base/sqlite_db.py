from config import bot, cur, base
from config import error_check as err
from aiogram import types


async def sql_addpizza_commands(state):
    async with state.proxy() as data:
        cur.execute('INSERT INTO pizzamenu VALUES(%s, %s, %s, %s)', tuple(data.values()))
        base.commit()


async def sql_adddrink_commands(state):
    async with state.proxy() as data:
        cur.execute('INSERT INTO drinkmenu VALUES(%s, %s, %s, %s)', tuple(data.values()))
        base.commit()


async def sql_addsale_commands(state):
    async with state.proxy() as data:
        cur.execute('INSERT INTO salemenu VALUES(%s, %s, %s, %s)', tuple(data.values()))
        base.commit()


async def sql_readpizza(message: types.Message):
    comm = 'SELECT * FROM pizzamenu'
    res = err(base, comm)
    base.commit()
    for ret in res:
        await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nОписание:\n {ret[2]}\nЦена: {ret[-1]}')


async def sql_readdrink(message):
    comm = 'SELECT * FROM drinkmenu'
    res = err(base, comm)
    base.commit()
    for ret in res:
        await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nОписание:\n {ret[2]}\nЦена: {ret[-1]}')


async def sql_readsale(message):
    comm = 'SELECT * FROM salemenu'
    res = err(base, comm)
    base.commit()
    for ret in res:
        await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nОписание:\n {ret[2]}\nЦена: {ret[-1]}')


async def sql_readpizza_admin():
    comm = 'SELECT * FROM pizzamenu'
    res = err(base, comm)
    base.commit()
    return res


async def sql_readdrink_admin():
    comm = 'SELECT * FROM drinkmenu'
    res = err(base, comm)
    base.commit()
    return res


async def sql_readsale_admin():
    comm = 'SELECT * FROM salemenu'
    res = err(base, comm)
    base.commit()
    return res


async def sql_deletedrink_command(data):
    cur.execute('DELETE FROM drinkmenu WHERE name = %s', (data,))
    base.commit()


async def sql_deletepizza_command(data):
    cur.execute('DELETE FROM pizzamenu WHERE name = %s', (data,))
    base.commit()


async def sql_deletesale_command(data):
    cur.execute('DELETE FROM salemenu WHERE name = %s', (data,))
    base.commit()
