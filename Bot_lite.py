from aiogram.utils import executor
import config
import os
from config import base, cur, bot, dp
from litebot_handlers import client, admin, other


def sql_start():
    if base:
        print('Data base connect is OK')
    cur.execute('BEGIN;')
    cur.execute('CREATE TABLE IF NOT EXISTS pizzamenu (img TEXT, name TEXT PRIMARY KEY, description TEXT, price TEXT)')
    cur.execute('CREATE TABLE IF NOT EXISTS drinkmenu (img TEXT, name TEXT PRIMARY KEY, description TEXT, price TEXT)')
    cur.execute('CREATE TABLE IF NOT EXISTS salemenu (img TEXT, name TEXT PRIMARY KEY, description TEXT, price TEXT)')
    cur.execute('CREATE TABLE IF NOT EXISTS client_id_base (id TEXT, count INTEGER)')
    base.commit()

async def on_startup(dp):
    await bot.set_webhook(config.URL_APP)
    sql_start()


async def on_shutdown(dp):
    await bot.delete_webhook()
    cur.close()
    base.close()


client.register_hnd_client(dp)
admin.register_hnd_admin(dp)
other.register_hnd_other(dp)

executor.start_webhook(
    dispatcher=dp,
    webhook_path='',
    on_startup=on_startup,
    on_shutdown=on_shutdown,
    skip_updates=True,
    host="0.0.0.0",
    port=int(os.environ.get("PORT", 5000)))
