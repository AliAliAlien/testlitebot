from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


info_button = InlineKeyboardButton(text='Информация',callback_data='Информация')
info_button1 = InlineKeyboardButton(text='Расписание',callback_data='Расписание')
info_button2 = InlineKeyboardButton(text='Расположение',callback_data='Расположение')
info_button3 = InlineKeyboardButton(text='Контактная информация',callback_data='Контактная информация')

menu_button = InlineKeyboardButton(text='Меню',callback_data='Меню')
menu_button1 = InlineKeyboardButton(text='Пицца',callback_data='Пицца')
menu_button2 = InlineKeyboardButton(text='Напитки',callback_data='Напитки')
menu_button3 = InlineKeyboardButton(text='Акции',callback_data='Акции')

return_button = InlineKeyboardButton(text='Назад',callback_data='Назад')

kb_client_menu = InlineKeyboardMarkup()

kb_client_info = InlineKeyboardMarkup()

kb_client = InlineKeyboardMarkup()

kb_client_order = InlineKeyboardMarkup()

kb_client_info.add(info_button1).insert(info_button2).add(info_button3).add(return_button)

kb_client.row(info_button).add(menu_button)

kb_client_menu.row(menu_button1, menu_button2, menu_button3).add(return_button)


