from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

button_load_pizza = KeyboardButton('/Загрузить_пиццу')
button_load_drink = KeyboardButton('/Загрузить_напиток')
button_load_sale = KeyboardButton('/Загрузить_акцию')
button_delete_pizza = KeyboardButton('/Удалить_пиццу')
button_delete_drink = KeyboardButton('/Удалить_напиток')
button_delete_sale = KeyboardButton('/Удалить_акцию')
button_cancel = KeyboardButton('/Отмена')

kb_admin = ReplyKeyboardMarkup(resize_keyboard=True).add(button_load_pizza).insert(button_load_drink) \
    .insert(button_load_sale).add(button_delete_pizza).insert(button_delete_drink).insert(button_delete_sale) \
    .add(button_cancel)
